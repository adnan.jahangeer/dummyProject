#include<iostream>
#include<string>
using namespace std;


bool isPalindrome(string input)
{
    int length;
    length = input.length();

    for (int i = 0; i < length/2; i++)
    {
        if (input[i] != input[length-i-1])
        {
            return false;
        }
    }

    return true;
}


int main()
{
    string input = "abccbba";
    cout << "Enter a string to check Palindrome: ";
    cin >> input;
    if (isPalindrome(input))
    {
        cout  << "Entered string is a Palindrome\n";
    }
    else
    {
        cout  << "Entered string is not a Palindrome\n";
    }
    
    
}