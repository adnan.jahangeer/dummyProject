#include<iostream>
#include<fstream>
#include<string>
#include<cstring>
using namespace std;


int findIndex(char roman_numeral[], char toFind)
{
    for(int i = 0; i < 7; i++)
    {
        if(toFind == roman_numeral[i])
        {
            return i;
        }
    }
    return -1;
}

int ConvertRomanToNumeric(string input)
{
    char roman_numeral[7] = {'M', 'D', 'C', 'L', 'X', 'V', 'I'};
    int weights[7] = {1000,500,100,50,10,5,1};
    int temp = 0;
    int temp2 = 0;
    int result = 0;
    for(int i = 0; i < input.length(); i++ )
    {
        temp = findIndex(roman_numeral, input[i]);
        bool flag = false;
        if(i+1 < input.length())
        {
            temp2 = findIndex(roman_numeral, input[i+1]);
            if (temp > temp2)
                flag = true;
            
        }
        if(temp != -1 && temp2 != -1)
        {
            if (!flag)
            {
                result += weights[temp];
            }
            else
            {
                result = result + weights[temp2] - weights[temp];
                i += 1;
            }
        }  
        else
        {
            throw logic_error("Invalid input");
        }
    }
    return result;

}

int main()
{
    try
    {
        string input;
        cout << "Enter input: \n";
        cin >> input ;
        cout << "Numeral: " << ConvertRomanToNumeric(input) <<endl;
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }
    
}

