#include<iostream>
#include<memory>
#include<array>
#include<string>
#include<cstring>
#include<map>
#include<algorithm>

using namespace std;

template<class T1= string, int T2=10>
class CircularBuffer{
    private:
        array<T1, T2> data;
        int readIndex;
        int writeIndex;
        int length;
    public:
        CircularBuffer()
        {
            readIndex = 0;
            writeIndex = 0;
        }

        void readArray()
        {
            if (readIndex < writeIndex)
            {
                auto temp = (data[(readIndex%T2)]);
                data[(readIndex%T2)] = move(*(make_shared<T1>()));
                readIndex += 1; 
                cout << "Data is: " <<  temp << endl;;
                cout << endl;
            }
            else if ((writeIndex%T2) == (readIndex%T2))
            {
                cout << "Nothing to read" <<endl;
            }
        }

        void writeArray()
        {
            cout << "Enter value to write: "; 
            T1 inp;
            cin >> inp;
            cout << endl;
            bool flag = false;
            if ((writeIndex%T2) != (readIndex%T2))           
                flag = true;
            else
                if (writeIndex == readIndex)
                    flag = true;
                
            if (flag)
            {
                cout << "Index is :" << (writeIndex%T2) << endl;
                data[(writeIndex%T2)] = inp;
                writeIndex += 1;
                printall();
            }
            else
                cout << "Buffer is full\n"; 
        }

        void searchArray()
        {
            cout << "Enter what you want to search: ";
            T1 inp;
            cin >> inp;
            cout << endl;
            auto it1 = find_if(data.begin(), data.end(), [&inp](string a)
            {
                return (a==inp);
            });

            if (it1 != end(data))
            {
                cout << "Found" <<endl;
            }
            else
            {
                cout << "Not found" <<endl;
            }
            
        }

        void printall()
        {
            for (auto value: data)
                cout << value << " ";
            cout << endl; 
        }
};

int main()
{
    int size, read=0, write=0;
    char input = ' ';
    auto x = new CircularBuffer<string>();


    while(input != 'q')
    {
        cout << "Enter any of the following: \n"
        << "\'r\' to read a value from buffer \n"
        << "\'w\' to write a value from buffer \n"
        << "\'s\' to search a value in buffer \n"
        << "\'q\' to quit\n"
        << "-------------------\n\n";
        cin >> input ;
        switch(input)
        {
            case 'r':
                x->readArray();
                break;
            case 'w':
                x->writeArray();
                break;
            case 's':
                x->searchArray();
                break;
        }
        x->printall();
        cout << endl;
    }
    
    return 0;
}