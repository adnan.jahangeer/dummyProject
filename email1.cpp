#include<iostream>
#include<string>
using namespace std;

bool checkValid(string input)
{   
    bool check_at = false, check_atdot = false, nospace = true;
    for(int i = 0; i < input.length(); i++)
    {
        if(input[i] == '@')
        {
            if (i != 0)
                check_at = true;
        }
        else if(input[i] == ' ')
        {
            nospace = false;
            break;
        }
        else if(input[i] == '.')
        {
            if (check_at)
            {
                if (!check_atdot)
                    check_atdot = true;
                else
                {
                    check_atdot = false;
                    break;
                }
            }
            
        }
    }
    if (check_at && nospace && check_atdot)
        return true;
    else
    {
        return false;
    }
    
}

int main()
{
    string input;
    cout << "Enter email to check if valid: ";
    cin >> input;
    if (checkValid(input))
    {
        cout << "Email is valid\n";
    }
    else
    {
        cout << "Invalid email address\n";
    }
    
}